import logo from './logo.svg';
import './App.css';

import Header from './components/Header';
import AboutUsPages from './pages/AboutUsPage';
import FortunePage from './pages/FortunePage';

import {Routes,Route} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
        <Routes>

        <Route path = "/lucky" element = {
                <FortunePage/>
                } />
          <Route path="/" element={
                  <AboutUsPages/>
                  }/>
          
          
          
        
        </Routes>
    </div>
  );
}

export default App;

