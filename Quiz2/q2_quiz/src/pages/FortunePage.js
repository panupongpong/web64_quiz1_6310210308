
import { useState } from "react";
import Fortune from "../components/Fortune";

import Button from '@mui/material/Button';
import Grid  from "@mui/material/Grid";
import { Box, Container, Typography } from "@mui/material";


function FortunePage (){

    const[name,setName] = useState("");
    const [guessResult , setGuessResult ] =useState("");
    const[ old, setOld] = useState("")
    const[ year, setYear] = useState("")
    const [tranFor,setTranFor] = useState("")


    function CalFor(){
        let o = parseInt(old)
        let y = parseInt(year)
        let guess = y/(o*10);
        setGuessResult(guess);
        if (guess < 15 ){
            setTranFor("fortunate")
        }
        else {
            setTranFor("bad luck")
        }
    }

   
    return(
        <Container maxWidth='lg'> 
        <Grid container spacing={2} sx={ { marginTop :"10px" } }>
            <Grid item xs={12}>
                <Typography variant="h4"> 
                Welcom to fortune telling
                </Typography>
            </Grid>
            <Grid item xs={8}>
                <Box sx ={{ textAlign : 'center' }}>
                              <br />
                              <br />
                Name: <input type = "text"
                              value={name}
                              onChange={ (e) => { setName(e.target.value) } } /> 
                              <br />
                              <br />
                Old: <input type = "text" 
                             value={old}
                             onChange={ (e) => { setOld(e.target.value) } } /> 
                             <br />
                             <br />
                Year of birth: <input type = "text" 
                            value={year}
                             onChange={ (e) => { setYear(e.target.value) } } /> 
                             <br />
                             <br />
                             
                             <Button variant="contained"onClick={ ()=>{ CalFor () } }>
                             Guess
                             </Button>
                          
                </Box>
            </Grid>
            <Grid item xs={2}>
                {  guessResult !=  0 &&
                        <div>
                            <br />
                            Prediction
                            <Fortune
                            name = { name }
                            result = { tranFor }
                            />
                        </div>

                    } 
            </Grid>
         </Grid> 
    </Container>
);   
}
export default FortunePage;

/*div align = "left">
            <div align = "center">
            Welcom to fortune telling  
                <hr/>
                Name: <input type = "text"
                              value={name}
                              onChange={ (e) => { setName(e.target.value) } } /> 
                              <br />
                              <br />
                Old: <input type = "text" 
                             value={old}
                             onChange={ (e) => { setOld(e.target.value) } } /> 
                             <br />
                             <br />
                Year of birth: <input type = "text" 
                            value={year}
                             onChange={ (e) => { setYear(e.target.value) } } /> 
                             <br />
                             <br />
                             
                             <Button variant="contained"onClick={ ()=>{ CalFor () } }>
                             Guess
                             </Button>
            <br />
            <br />
            
            Prediction
            <Fortune
            name = { name }
            result = { tranFor }
            />                    
            </div>
        </div>
*/ 
