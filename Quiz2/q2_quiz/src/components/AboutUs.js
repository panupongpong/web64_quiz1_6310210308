import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

function AboutUs (props){

    return (
    <Box sx= {{ flexGrow : 1, width: "100%" }} > 
        <Box sx={{ justifyContent : 'center',margin : "20px", width : "100%", display : "flex"}}>
              <Card sx={{ Width: 275  }}>
                        <CardContent>
                             <Typography  sx={{ fontSize: 20 }} >
                             <p>จัดทำโดย : {props.name} </p>
                             <p>ติดต่อได้ที่ : {props.address}</p>
                             <p>ที่อยู่ : {props.province}</p>

                              </Typography>
                          </CardContent>          
              </Card>
            
        </Box>
    </Box>
      
    );
}
export default AboutUs;


