
import {Link} from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h6"component="div"> 
            Welcom to fortune telling :
            </Typography>
            &nbsp;&nbsp;
            <Link to="/lucky">fortune telling </Link> 
            &nbsp;&nbsp;
             <Link to="/">
                 <Typography variant="body1">
                 organizer
                </Typography>
            </Link>
           
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;
